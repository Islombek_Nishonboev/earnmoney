package net.makemoney.android.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import net.makemoney.android.R
import net.makemoney.android.adapters.holders.DetailsSimpleViewHolder
import net.makemoney.android.adapters.holders.LoadingHolder
import net.makemoney.android.adapters.holders.ProgressViewHolder
import net.makemoney.android.data.models.details.DetailsItem
import net.makemoney.android.data.models.details.DetailsSimpleItem
import net.makemoney.android.data.models.details.ProgressItem
import net.makemoney.android.extensions.inflate
import net.makemoney.android.utils.pagination.PaginationAdapter


class CombinedDetailsAdapter(private val items: ArrayList<DetailsItem>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>(), PaginationAdapter<DetailsItem> {

    override fun getItemViewType(position: Int): Int = when {
        items[position].id < 0 -> R.layout.item_loading_holder
        items[position] is ProgressItem -> R.layout.item_details_progress
        else -> R.layout.item_details_simple_info
    }


    //Change November 22 changed return type from : DetailsSimpleViewHolder to RecyclerView.ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        R.layout.item_loading_holder -> LoadingHolder(parent.inflate(R.layout.item_loading_holder))
        R.layout.item_details_progress -> ProgressViewHolder(parent.inflate(R.layout.item_details_progress))
        R.layout.item_details_simple_info -> DetailsSimpleViewHolder(parent.inflate(R.layout.item_details_simple_info))
        else -> throw IllegalArgumentException("Wrong active task item type")
    }


    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = when (holder) {
        is ProgressViewHolder -> holder.bind(items[position] as ProgressItem)
        is DetailsSimpleViewHolder -> holder.bind(items[position] as DetailsSimpleItem)
        is LoadingHolder -> Unit
        else -> throw IllegalArgumentException("Wrong active task holder type")
    }

    override fun addLoadingFooter() {
        val holderPosition = items.size
        items.add(DetailsItem(-1))
        notifyItemInserted(holderPosition)
    }

    override fun removeLoadingFooter() {
        val holderPosition = items.size - 1
        //removes item only if loading footer was added
        if (holderPosition >= 0) {
            items.removeAt(holderPosition)
            notifyItemRemoved(holderPosition)
        }
    }

    override fun addItems(newItems: List<DetailsItem>) {
        clearItems()
        val from = items.size
        items.addAll(newItems)
        notifyItemRangeInserted(from, newItems.size)
    }

    override fun clearItems() {
        items.clear()
        notifyDataSetChanged()
    }
}