package net.makemoney.android.data.models.entertainment

import android.os.Build
import android.os.Parcel
import androidx.annotation.RequiresApi
import net.makemoney.android.R
import net.makemoney.android.data.models.tasks.TaskItem
import net.makemoney.android.extensions.parcelableCreator
import net.makemoney.android.extensions.readBoolean
import net.makemoney.android.extensions.writeBoolean
import net.makemoney.android.utils.Parcelize


data class QuizTaskItem(
    override val id: Int,
    val title: String,
    val imageUrl: String,
    val todayTimes: Int,
    val limit: Int,
    val isAvailable: Boolean,
    val background: Int = R.drawable.bg_entertainment_card_1
) : net.makemoney.android.data.models.tasks.TaskItem(), Parcelize {

    @RequiresApi(Build.VERSION_CODES.Q)
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt() == 1,
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) = with(parcel) {
        writeInt(id)
        writeString(title)
        writeString(imageUrl)
        writeInt(todayTimes)
        writeInt(limit)
        writeInt(if (isAvailable) 1 else 0)
        writeInt(background)
    }

    companion object {
        @JvmField
        val CREATOR = parcelableCreator(::QuizTaskItem)
    }
}